/* All columns */
SELECT * FROM `Track`;

/* Some columns */
SELECT `Name`, `Composer`
FROM `Track`;

/* Some rows/columns */
SELECT `Name`, `Composer`
FROM `Track`
WHERE `Composer` LIKE "Jimmy%";

/* Counting */
SELECT count(*)
FROM `Track`
WHERE `Composer` LIKE "Jimmy%";

/* No repeats */
SELECT DISTINCT `Composer`
FROM `Track`;

/* Ordering */
SELECT * 
FROM `Artist`
ORDER BY `Name`;

/* Column aliases */
SELECT count(*) AS `JimmyCount`
FROM `Track`
WHERE `Composer` LIKE "Jimmy%";

/* Grouping */
SELECT `Composer`, count(`TrackId`) AS `ComposerTrackCount`
FROM `Track`
GROUP BY `Composer`;

/* Group filtering */
SELECT `Composer`, count(`TrackId`) AS `ComposerTrackCount`
FROM `Track`
GROUP BY `Composer`
HAVING `ComposerTrackCount` > 10;

/* Inner join */
SELECT `Track.Name`, `Album.Title`
FROM `Album`
INNER JOIN `Track` ON `Track.AlbumId` = `Album.AlbumId`;

/* Inner join with table aliases */
SELECT T.`Name`, A.`Title`
FROM `Track` T
INNER JOIN `Album` A ON T.`AlbumId` = A.`AlbumId`;

/* Outer join */
SELECT T.`Name`, G.`Name`
FROM `Track` T
RIGHT OUTER JOIN `Genre` G ON T.`GenreId` = G.`GenreId` 
Where  T.`Name` IS NULL;

